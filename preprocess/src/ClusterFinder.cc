/* 
 * File:   ClusterFinder.cc
 * Author: mareike weers
 * 
 * Created september 2016
 */

#include "ClusterFinder.h"

void ClusterFinder::buildEvent(TBCore* core, TBEvent* event)
{
	event->fRawClusters = kBad;
	event->fClusters = kBad;
	
	ClusterFinder::findClusters(event);
	
	if(event->rawClusters.size() != 0)
	{
		event->fRawClusters = kGood;
	}
	
	if(event->clusters.size() != 0)
	{
		event->fClusters = kGood;
	}
}

void ClusterFinder::findClusters(TBEvent* event)
{
	TBCluster* newCluster;
	TBHit* newHit;
	
	bool found;
	bool added;
	
        int clustercounter;
	TBCluster* pointerToCluster;
        std::vector<int>  deleteCluster;

	if (event->rawHits.size()>0)
	{	
		//first element is cluster seed
		event->addRawCluster();
		newCluster=event->rawClusters.back();

		newHit=event->rawHits.front();
		newCluster->hits.push_back(newHit);

		ClusterFinder::firstHitCheck(newCluster, newHit);

		auto rawHit = event->rawHits.begin();
		rawHit++;
		//loop over all rawHits
		while (rawHit !=event->rawHits.end())
		{	
			added = false;
			pointerToCluster=NULL;
			clustercounter=0;
			deleteCluster.clear();
					
			//Loop over all RawClusters
			auto cluster=event->rawClusters.begin();
			while (cluster!=event->rawClusters.end())
			{	
				found = false;
				//Loop over all hits in every RawCluster (from end to begin)
				for(auto clusterHits=(*cluster)->hits.crbegin(); clusterHits!=(*cluster)->hits.crend(); ++clusterHits)
				{	//check if neighbor
					if (std::abs( (*clusterHits)->col - (*rawHit)->col) > 1 || std::abs( (*clusterHits)->row - (*rawHit)->row) > 1)
					{
						continue;
					}
					else
					{ 
						found=true;
						break;
					}
				}

				//when hit already added and fits also to another cluster: merge clusters
				if ( found == true && added == true)
				{
					//go through all entries in actual cluster
					for(auto neighborHits: (*cluster)->hits)
					{
						//and add them to the other cluster
						pointerToCluster->hits.push_back(neighborHits);
						ClusterFinder::hitCheck(pointerToCluster, neighborHits);
					}

					(*cluster)->hits.clear();
					deleteCluster.push_back(clustercounter);
				}
        	                //when hit matches first time: insert to cluster
        	                if (found == true && added == false)
        	                {
        	                        (*cluster)->hits.push_back( (*rawHit) );

					ClusterFinder::hitCheck(*cluster, *rawHit);

                	                added=true;
                	                pointerToCluster=(*cluster);
                	        }
				cluster++;
				clustercounter++;
			}
		
			//mark empty clusters (after merging) as bad clusters
			if (deleteCluster.size()>0) 
			{
				for (auto i: deleteCluster)
				{
					(event->rawClusters)[i]->fClusterGood = kBad;
				}
	                        deleteCluster.clear();
			}

			//when no neighbor is found: new cluster seed
			if (found==false && added==false)
			{	
    				event->addRawCluster();
        			newCluster=event->rawClusters.back();
        			newCluster->hits.push_back( (*rawHit) );
				
				ClusterFinder::firstHitCheck(event->rawClusters.back(), *rawHit);
			}
			rawHit++;
		}

		//only add good clusters to event->clusters
		for (auto tbcluster : event->rawClusters)
		{
			if ((tbcluster)->fClusterGood == kGood)
			{
				event->clusters.push_back(tbcluster);
	
			}
		}
		
	}//end if rawHits.size!=empty 
}


void ClusterFinder::firstHitCheck(TBCluster* cluster, TBHit* hit)
{
	if(hit->fCentralHit == kGood)
	{
        	cluster->fCentralPixelCluster = kGood;
                cluster->fEdgePixelCluster = kBad;
                cluster->fClusterGood = kGood;
	}
        if(hit->fEdgeHit == kGood)
        {
        	cluster->fCentralPixelCluster = kBad;
                cluster->fEdgePixelCluster = kGood;
                cluster->fClusterGood = kGood;
	}
        if(hit->fMaskedHit == kGood)
        {
        	cluster->fMaskedPixelCluster = kGood;
                cluster->fClusterGood = kBad;
	}
}

void ClusterFinder::hitCheck(TBCluster* cluster, TBHit* hit)
{
	if(hit->fEdgeHit)
        {
        	cluster->fCentralPixelCluster = kBad;
                cluster->fEdgePixelCluster = kGood;
	}
        if(hit->fMaskedHit)
        {
        	cluster->fMaskedPixelCluster = kGood;
                cluster->fClusterGood = kBad;
	}
}
