#ifndef TOTCALIB_H
#define TOTCALIB_H

#include <string>
#include <vector>
#include <cstdlib>
#include <string>
#include "TF1.h"
#include "TH2F.h"
#include "TFile.h"
#include "TROOT.h"

#include <iostream>

class ToTCalib {
  
 private:
  // did everything work loading the histograms?
  bool histLoaded;
  // is there an own calibration for each pixel?
  bool perPixelCalib;
  
  //coefficients for polynomial tot function (for every single Pixel)
  std::vector<TH2F*> h_par;
  //polynomial tot function
  TF1 *function;

 public:
	 ToTCalib(){};
  ToTCalib(std::string filename);
  virtual ~ToTCalib();
  virtual double q(int tot, int col, int row);

 virtual bool hasPerPixelCalib() { return perPixelCalib; }
 virtual bool hasCalib() { return histLoaded; }

};

#endif //TOTCALIB_H
