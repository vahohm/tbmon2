/* 
 * File:   TBEvent.h
 * Author: Daniel Kalin
 *
 * Created on 23. Januar 2014
 * 
 * v1.0
 */

#ifndef TBEVENT_H
#define	TBEVENT_H

#include <limits>
#include <vector>

#include "TBDut.h"
#include "TBCluster.h"

enum eventflag_t
{
	kBad,
	kGood,
	kUnknown
};

class TBTrack;
class TBEvent;
	
class TBHit
{
public:
	int trig; // Trigger number
	int iden; // Module identifier
	int row;  // Pixel row 
	int col;  // Pixel column
	int tot;  // Channel ToT (0-255)
	int lv1;  // BX id relative to lv1 (0-15)
	
	
	eventflag_t fMaskedHit; // Hit is on a pixel with mask
	eventflag_t fEdgeHit; // Hit is on an edge pixel
	eventflag_t fCentralHit; // Hit is on a center pixel
	eventflag_t fLv1; // Hit passed lv1 cut
	/**
	 * place for some flags
    */
	
	TBHit();
	
	~TBHit();
	
	int getLv1();
};

class TBCluster
{
public:
	std::vector<TBHit*> hits;
	
	eventflag_t fMaskedCluster; // Masked cluster
	eventflag_t fEdgePixelCluster; // Cluster with min one edge pixel (fCentralPixelCluster = kBad)
	eventflag_t fCentralPixelCluster; // Cluster with only center pixel (fEdgePixelCluster = kBad)
	eventflag_t fMaskedPixelCluster; // Cluster with min one maske pixel
	eventflag_t fClusterGood; // Cluster without mask pixel
	
	/**
	 * place for some flags
    */
	
	TBCluster();
	
	~TBCluster();
	
	// static
	// All clusters
	static int getSumToT(const TBCluster* tbcluster);
	static int getSumCharge(const TBCluster* tbcluster, const TBEvent* event);
	
	//The following methods returns the index of a cluster
	static TBCluster* getMaxToTCluster(const std::vector<TBCluster*>* clusters);
	static TBCluster* getMatchedCluster(const std::vector<TBCluster*>* clusters, const TBTrack* tbtrack, const TBEvent* event, double matchX =  0.0, double matchY = 0.0);
	
	//Does track match pixel hit?
	static bool isMatch(const TBCluster* tbcluster, const TBTrack* tbtrack, const TBEvent* event);
	//static bool isCentralRegion(const TBCluster* tbcluster, const TBEvent* event);
	
	// Single cluster, returns index of cell
	static TBHit* getMaxToTCell(const TBCluster* tbcluster);

	//********One dimensional position finding algorithms**************///
	//Cols and rows
	static double getEtaCorrectedCol(const TBCluster* tbcluster, const TBEvent* event);
	static double getEtaCorrectedRow(const TBCluster* tbcluster, const TBEvent* event);
	//digital head-tail (DHT) PFA
	static double getClusterCenterColByDigitalHeadTail1D(const TBCluster* cluster, const TBEvent* event);
	static double getClusterCenterRowByDigitalHeadTail1D(const TBCluster* cluster, const TBEvent* event);
	//analog head-tail (AHT) PFA
	static double getClusterCenterColByAnalogHeadTail1D(const TBCluster* tbcluster, const TBEvent* event, int signal_0, double t, double theta);
	static double getClusterCenterRowByAnalogHeadTail1D(const TBCluster* tbcluster, const TBEvent* event, int signal_0, double t, double phi);
	//one-sided analog head-tail (1S-AHT) PFA
	static double getClusterCenterColByOneSidedAnalogHeadTail1D(const TBCluster* tbcluster, const TBEvent* event, int headSide, int signal_0, double t, double theta);
	static double getClusterCenterRowByOneSidedAnalogHeadTail1D(const TBCluster* tbcluster, const TBEvent* event, int headSide, int signal_0, double t, double phi);

	// X and Y in µm
	static double getUnWeightedX(const TBCluster* cluster, const TBEvent* event);
	static double getUnWeightedY(const TBCluster* cluster, const TBEvent* event);
	//center-of-gravity
	static double getChargeWeightedX(const TBCluster* cluster, const TBEvent* event);
	static double getChargeWeightedY(const TBCluster* cluster, const TBEvent* event);

	//todo until there is a better solution: take chargeWeighted instead of etaCorrected
	static double getEtaCorrectedX(const TBCluster* tbcluster, const TBEvent* event);
	static double getEtaCorrectedY(const TBCluster* tbcluster, const TBEvent* event);
  
	// using the 2d histogram (charge map), created for every pixel
	static double getClusterCenter(const TBCluster* tbcluster, const TBEvent* event, double* x, double* y, double* uncertaintyX, double* uncertaintyY);
	//digital head-tail (DHT) PFA
	static double getClusterCenterXByDigitalHeadTail1D(const TBCluster* cluster, const TBEvent* event);
	static double getClusterCenterYByDigitalHeadTail1D(const TBCluster* cluster, const TBEvent* event);
	//analog head-tail (AHT) PFA
	static double getClusterCenterXByAnalogHeadTail1D(const TBCluster* tbcluster, const TBEvent* event, int signal_0, double t, double theta, double phi);
	static double getClusterCenterYByAnalogHeadTail1D(const TBCluster* tbcluster, const TBEvent* event, int signal_0, double t, double theta, double phi);
	//one-sided analog head-tail (1S-AHT) PFA
	static double getClusterCenterXByOneSidedAnalogHeadTail1D(const TBCluster* tbcluster, const TBEvent* event, int headSideCol, int headSideRow, int signal_0, double t, double theta, double phi);
	static double getClusterCenterYByOneSidedAnalogHeadTail1D(const TBCluster* tbcluster, const TBEvent* event, int headSideCol, int headSideRow, int signal_0, double t, double theta, double phi);

	//*****************************************************************///

	static int spannedRows(const TBCluster* tbcluster, const TBEvent* event);
	static int spannedCols(const TBCluster* tbcluster, const TBEvent* event);

	static double getInClusterMaxPitchX(const TBCluster* tbcluster, const TBEvent* event);
	static double getInClusterMaxPitchY(const TBCluster* tbcluster, const TBEvent* event);
};

class TBTrack
{
public:
	int trig; // Trigger number
	int iden; // Modul identifier
	int trackNum; // Track number
	double trackX; // Track x position
	double trackY; // Track y position
	int trackCol; // Track column
	int trackRow; // Track row
	double dxdz;
	double dydz;
	double chi2;
	double ndof;
	double zpos; // zPos of DUT (euhits)
	
	TBHit* matchedHit;
	int matchedHitType; // to which kind of hit the track is matched
	/** all possible types for matchedHitType
	 * 1 - Track position inside an unmasked and center hit
	 * 2 - Track position inside the margin of an unmasked and center hit with min distance to pixel center
	 * 3 - Track position inside an unmasked and edge hit
	 * 4 - Track position inside the margin of an unmasked and edge hit with min distance to pixel center
	 * 5 - Track position inside a masked and center hit
	 * 6 - Track position inside the margin of a masked and center hit with min distance to pixel center
	 * 7 - Track position inside a masked and edge hit
	 * 8 - Track position inside the margin of a masked and edge hit with min distance to pixel center
	 * 9 - Track is not matched to any hit
	 */
	TBCluster* matchedCluster;
	
	eventflag_t fMatchedHit;
	eventflag_t fMatchedCluster;
	eventflag_t fMatchedTrack;
	eventflag_t fTrackChi2; // Is track Chi2 accepted? ///< (CHI2 12) 
	eventflag_t fTrackEdgeRegion; // kBad if track passes through central pixel
	eventflag_t fTrackCentralRegion;  // kBad if track passes through an edge pixel
	eventflag_t fTrackMaskedRegion;  // kGood if the track matches a pixel which is masked out by DUT mask 
	eventflag_t fEtaCorrections; ///< CLUSTF 24 
	eventflag_t fEtaCut;  //BAT eta cuts ///< (ETACUT 43 *******due to a presumed error line 46, the flag is never set kGood********) 
	
	/**
	 * place for some more flags and other stuff
    */
	
	TBTrack();
	
	~TBTrack();
};

class TBEvent
{
public:
	// Base
	DUT* dut;
	int iden;
	
	// Hits
	std::vector<TBHit*> rawHits; // All hits from TBTrackfile
	std::vector<TBHit*> hits; // Hits passed lv1 cut
	
	// Clusters
	std::vector<TBCluster*> rawClusters; // Cluster of rawHits
	std::vector<TBCluster*> clusters; // Cluster of hits
	
	// Tracks
	std::vector<TBTrack*> rawTracks; // all tracks 
	std::vector<TBTrack*> tracks; //
	
	// Flags
	eventflag_t fRawHits;
	eventflag_t fHits; // now it used
	eventflag_t fRawClusters;
	eventflag_t fClusters; // Are clusters built? kGood if at least one cluster build in ClusterFinder (i.e. at least one hit for this trigger ID)
	eventflag_t fRawTracks;
	eventflag_t fTracks; // by default kUnknown. If at least one track of the trigger ID has sufficient matched hits in EuBuildTrack, set to kGood 
	
	TBEvent(DUT* dut);
	~TBEvent();
	void clear();
	void addRawHit(int trig, int iden, int col, int row, int tot, int lv1);
	void addRawTrack(int trig, int iden, int trackNum, double trackX, double trackY, double dxdz, double dydz, double chi2, double ndof);
	void addRawCluster();
	
private:
	// Event memory management
	bool clearedRawHits;
	std::vector<TBHit*> storedHits;
};

#endif	/* TBEVENT_H */

