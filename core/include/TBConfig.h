/* 
 * File:   TBConfig.h
 * 
 * Created on 25. April 2013
 */
#ifndef TBCONFIG_H
#define	TBCONFIG_H

// default libs and system libs
#include <assert.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <sys/stat.h>
#include <vector>

// ROOT libs, it should be installed on the system
//#include <TCanvas.h>
//#include <TFile.h>
//#include <TH1.h>
//#include <TTree.h>

// other

// TBmon classes
#include "TBDutConfig.h"

// TBConfig

class TBConfig
{
public:
	// draft config file name
	static std::string mainConfigName;
	static std::string analysisConfigName;
	// raw data pathsettings
	std::string rawDataPath;
	std::string rawDataName;
	static std::string rawDataNameExtension;
	// runs over all raw data with the same name
	std::vector<int> rawDataRuns;
	// number of Trigger which are view in one run
	int maxNumOfTrigToBeProcPerRun;
	// other settings
	int logLevel;
	bool organizeOutput;
	bool logToFile;
	// shated libs paths
	static std::string sharedEventBuilderPath;
	static std::string sharedPreprocessPath;
	static std::string sharedAnalysisPath;
	// output path
	std::string outputPath;
	static std::string preprocessingDataPath;
	static std::string plotPath;
	static std::string configPath;
	static std::string extMaskPath;
	std::string plotExtension;
	// preprocessing setting
	bool preprocessing;
	bool savePreprocessingFile;
	std::string addingPreprocessingFile;
	bool usePreprocessingFile;
	// overwrite preprocessing and plot files
	bool overwriteFiles;
	// use z-positions from reco
	bool useRecoZ;
	// used GEAR to describe the geometry for reconstruction
	bool usedGEAR;
	// use atlas style
	bool useAtlasStyle;
	// use eventbuilder
	std::vector<std::string> useEventBuilder;
	// use analyses
	std::vector<std::string> useAnalysis;
	// use duts
	std::map<std::string, std::string> useDUT;
	// preprocessing set up
	bool useCheckAlign;
	bool useGetEtaCorr;
	//bool clustering;
	//bool eventbuilder;
	//bool hotpixel;
	// plots
	bool inpixelMap;
	bool chipMap;
	bool centralPixelPlot;
	bool profile;
	bool checkalign;
	// general path setting
	static std::string generalDutConfigPath;
	static std::string draftConfigPath;
	// configs
	std::vector<TBDutConfig> dutConfig;
	// params for all DUT
	// form map< iden, map< EventBuilder, map< ParamName, ParamValue >>>
	std::map<int, std::map<std::string , std::map<std::string, std::string> > > param;

};
#endif	/* TBCONFIG_H */

