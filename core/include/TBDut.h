#ifndef TBDUT_H
#define TBDUT_H

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <cmath>
#include <limits>
#include <TApplication.h>

#include "TBDutConfig.h"
//#include "e4_totcalib.h"
#include "etacorrections.h"
//#include "root_totcalib.h"
#include "totcalib.h"

//std::string name = "TBDut";

class Rectangle
{
private:
	// defines the coordinates of lower left and upper right corner of the rectangle in mu m
	double lowerLeftX, lowerLeftY;
	double upperRightX, upperRightY;
	double area;

public:
	Rectangle(double lowerLeftX, double lowerLeftY,
				 double upperRightX, double upperRightY);
	bool inside(double x, double y);  // checks, if x,y is inside the rectangle
	bool inside(double x, double y, double marginX, double marginY);

	//getter methods
	double getLowerLeftX() { return lowerLeftX; }
	double getLowerLeftY() { return lowerLeftY; }
	double getUpperRightX() { return upperRightX; }
	double getUpperRightY() { return upperRightY; }
	double getArea() { return area; }
};

class PixelGeometry
{
private:
	bool edge;
	int numElec;
	int mirroringX;
	double maxPitchX;
	double maxPitchY;
	double geometricCenterX;
	double geometricCenterY;
	double area;

	double ratioInside(int x, int y, int radius);
 
public:
	std::vector< Rectangle > rectangles;
	int nPixels; // number of Pixels with this geometry
	PixelGeometry(bool edge, int numElec, int periodMirroringX, std::vector<TBRectangleConfig> rectangles);
	bool inside(double x, double y);
	bool inside(double x, double y, double marginX, double marginY);
	// getter methods
	double getMaxPitchX() const { return maxPitchX; }
	double getMaxPitchY() const { return maxPitchY; }
	double getGeometricCenterX() { return geometricCenterX; }
	double getGeometricCenterY() { return geometricCenterY; }
	double getArea() { return area; }
	bool   isEdgePixel() { return edge; }
	int    getMirroringX() { return mirroringX; }
	int		getNumElec(){ return(numElec); }
};

class Pixel
{
private:
	// defines the lower left corner of a pixel (if ambiguous, take the lower left corner of a surrounding rectangle istead)
	double lowerLeftX, lowerLeftY;
	int mask;
	bool mirroring;
	PixelGeometry *geometry;

	int nMasks; // in 2^x, where x is the number of masks

	bool whichMask(int type);
public:
	Pixel();  // default constructor is called by pixelArray.resize()
	void setPixel(double lowerLeftX, double lowerLeftY, bool mirroring, PixelGeometry *geometry);
	void addMask(int type);
	bool inside(double x, double y);
	bool inside(double x, double y, double marginX, double marginY);

	// get all the different masks
	int getMask()              { return mask; }
	bool isNoisy()             { return whichMask(1); }
	bool isNeighbourNoisy()    { return whichMask(2); }
	bool isDead()              { return whichMask(4); }
	bool isConfigMasked()      { return whichMask(8); }
	bool isCalibrationMasked() { return whichMask(16); }

	// getter methods
	double getLowerLeftX() { return lowerLeftX; }
	double getLowerLeftY() { return lowerLeftY; }
	PixelGeometry *getGeometry() { return geometry; }
	bool getMirroring() { return mirroring; }
	double getGeometricCenterX();
	double getGeometricCenterY();
};

class DUT
{
private:
	double matchX; //wenn isMatch in der Clusterklasse fertig ist wieder entfernen
	double matchY; //wenn isMatch in der Clusterklasse fertig ist wieder entfernen
	double matchPixelMarginX; // matching radius X around a pixel
	double matchPixelMarginY; // matching radius Y around a pixel
	double zPos;
	int lv1Min, lv1Max;
	int noisyNeighborColRadius;
	int noisyNeighborRowRadius;
	bool maskDeadPixels;
	int maxTot;
	// TEST delete later
	int numElec;

	int ncols;
	int nrows;
	int nPixelGeometries;

	double dutPitchX;
	double dutPitchY;

	double maxPixelPitchX;
	double maxPixelPitchY;

	TH2D* h_DUT;

	void addMaskPair(int col, int row);

public:
	std::string name;
	int iden;
	std::vector< std::pair<int,int> > masks;

	//2d Array for the pixels
	std::vector< std::vector<Pixel> > pixelArray;
	std::vector< PixelGeometry* > pixelGeometries;

	ToTCalib* totcalib;

	DUT(TBDutConfig &dutConfig);
	//virtual ~DUT();
	void initRun(int currentRun);
	void addToTCalib(const std::string& filename);
	void addMask(int col, int row, int type);
	void addMask(int col, int row, int type, int neighborX, int neighborY);
	double q(const double& tot, const int& col,const int& row);

	void getColRow(double x, double y, int *col, int *row);
	void getColRow(double x, double y, double marginX, double marginY, int *col, int *row);
	bool matchNeighborPixel(double x, double y, int col, int row, double marginX, double marginY, int* neighborCol, int* neighborRow, bool allowMask);
	bool matchNeighborPixel(double x, double y, double marginX, double marginY, int *neighborCol, int* neighborRow);
	// getter methods
	int getNcols() const  { return(ncols); }
	int getNrows() const  { return(nrows); }
	double getDutPitchX() const  { return(dutPitchX); }
	double getDutPitchY() const  { return(dutPitchY); }
	double getMaxPixelPitchX() const { return(maxPixelPitchX); }
	double getMaxPixelPitchY() const { return(maxPixelPitchY); }
	int getNoisyNeighborColRadius() const { return noisyNeighborColRadius; }
	int getNoisyNeighborRowRadius() const { return noisyNeighborRowRadius; }
	bool getMaskDeadPixels() const { return maskDeadPixels; }
	int getMaxTot() const { return(maxTot);}
	double getMatchX() const { return(matchX); } //wenn isMatch in der Clusterklasse fertig ist wieder entfernen
	double getMatchY() const { return(matchY); } //wenn isMatch in der Clusterklasse fertig ist wieder entfernen
	double getMatchPixelMarginX() const { return(matchPixelMarginX); }
	double getMatchPixelMarginY() const { return(matchPixelMarginY); }
	double getlv1Min() const  { return(lv1Min); }
	double getlv1Max() const  { return(lv1Max); }
	double getzPos() const  { return(zPos); }
	bool isEdgePixel(int col, int row);
	int getMask(int col, int row);
	int getGeometryNumber(PixelGeometry *pixelGeometry);
	void getFoldedXY(const int col, const int row, const double trackX, const double trackY, double* trackFoldedX, double* trackFoldedY);
	void getElectrodeDistance(const int col, const int row, const double x, const double y, double* distX, double* distY);
	// TEST delete later
	int getNumElec(){ return(numElec); }
	
	// TODO etacorrection fuer nicht rechteckige pixel
	EtaCorrections ecorrs;
  
/*	
  //Explicit cast here to avoid all those warnings.
  //double q(const double& tot, const int& col=-1,const int&row=-1) { return totcalib->q((int) tot,col,row); }
  double q(const double& tot, const int& col=-1,const int&row=-1) { return roottotcalib->q((int) tot,col,row); }
*/
	//static bool checkDutConfig(TBDutConfig* dutConfig);
};

#endif //TBDUT_H
